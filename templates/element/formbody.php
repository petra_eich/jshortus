<?php
/**
 * @var $formcontent
 * @var $pagecolor
 * @var $items
 */

$inhalt = [
    'add' => [
        'titel' => 'New',
    ],
    'edit' => [
        'titel' => 'Edit',
    ],
    'view' => [
        'titel' => isset($formtitle) ? $formtitle : $formcontent->name,
    ],
];
$action = $this->request->getParam('action');

if ( $action != 'view' ) {

    echo $this->Form->create($formcontent, ['type' => 'file']);
}

if ( $action != 'add' ) {

    $id = $formcontent->id;
//        echo $id;
} ?>

    <div class="card border-<?= $pagecolor; ?> mb-3">

        <div class="card-header text-white bg-<?= $pagecolor; ?>">
            <h2><?= __($inhalt[$action]['titel']); ?></h2>
        </div> <?php

        if ( $action == 'view' ) { ?>

            <?= $this->element('viewfields',['items' => $items, 'viewcontent' => $formcontent]); ?>
            <?= $this->element('cardfooter', ['created' => $formcontent->created,]); ?> <?php
        } else { ?>

            <?= $this->element('formfields',['items' => $items]); ?>

            <div class="card-footer bg-transparent">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <?= $this->Form->button(__('Save'), [
                            'id' => 'btnSubmit',
                            'type' => 'submit',
                            'class' => 'btn btn-primary',
                        ]); ?>
                    </div>
                </div>
            </div> <?php
        } ?>
    </div> <?php

if ( $action != 'view' ) {

    echo $this->Form->end();
} ?>

