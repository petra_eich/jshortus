<?php
/**
 * @var $controller
 */ ?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="nav navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item<?= $controller == 'Plants' ? ' active' : '' ?>">
                <h3>
                    <a class="nav-link" href="<?= $this->Url->build(
                        [
                            'controller' => 'Images',
                            'action' => 'index',
                        ]); ?>">
                        Images
                    </a>
                </h3>
            </li>
        </ul>

        <ul class="nav navbar-nav ms-auto mb-2 mb-lg-0">

            <li class="nav-item<?= $controller == 'Pages' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Pages',
                        'action' => 'display',
                        'about',
                    ]); ?>">
                    about
                </a>
            </li>

            <li class="nav-item<?= $controller == 'Pages' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Pages',
                        'action' => 'display',
                        'cakeinfo'
                    ]); ?>">
                    Info
                </a>
            </li>

            <li class="nav-item<?= $controller == 'Pages' ? ' active' : '' ?>">
                <a class="nav-link" href="<?= $this->Url->build(
                    [
                        'controller' => 'Pages',
                        'action' => 'display',
                        'home',
                    ]); ?>">
                    Home
                </a>
            </li> 
        </ul>
    </div>
</nav>

