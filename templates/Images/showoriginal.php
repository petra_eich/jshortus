<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Image $image
 */
?>

<?php $id = $image->id;

echo $this->Html->image($imgitem['text'], [
    'alt' => $imgitem['alt'],
    'class' => $imgitem['class'],
]);

$imgsize = getimagesize('img/' . $imgitem['text']); ?>

<p class="text-center">
    Höhe/Breite(original): <?= $imgsize[1]; ?> px <?= $imgsize[0]; ?> px 
</p> 
