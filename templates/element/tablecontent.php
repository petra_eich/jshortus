<?php
/**
 * @var $tbldata
 * @var $tblitems
 * @var $tblcolor
 * @var $pagecolor
 * @var $model
 * @var $filter
 */
$color = (isset($tblcolor) ? $tblcolor : $pagecolor);

// to sort and paginate (in differtent models) siehe families/view
if ( isset($model) ) {
    $this->Paginator->options(['model' => $model]);
}
// to sort (in differtent models)  siehe families/view
if ( isset($filter) ) {
    $this->Paginator->options(['url' => $filter]);
} ?>

<div class="table-responsive">
    <table class="table table-striped table-hover">
        <?= $this->element('tableheader',['headers' => $tblitems, 'tblcolor' => $color,]); ?>
        <?= $this->element('tablebody',['tbldata' => $tbldata, 'tblitems' => $tblitems, 'tblcolor' => $color,]); ?>
    </table>
</div>