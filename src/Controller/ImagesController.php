<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Http\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{
    
    public function initialize(): void
    {
        parent::initialize();
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index() 
    {
        $key = $this->request->getQuery('key');
        
        if ( $key ) {
            
            $query = $this->Images->find('all')->where(
                ['title LIKE' => '%' . $key. '%'],
            );
        } else {
            
            $query = $this->Images;
        }
        
        $this->paginate = [
            'order' => [
                'title' => 'asc',
            ],
        ];
        $images = $this->paginate($query, [
            'sortableFields' => [
                'title', 
            ]
        ]);

        $tblitems = [
            [
                'name' => 'Name',
                'field' => [
                    'name' => 'title',
                ],
                'sort' => 'title',
            ],
            [
                'name' => 'Image',
                'field' => [
                    'name' => 'filename',
                    'type' => 'imagelink',
                    'alt' => 'name',
                    'class' => 'img-thumbnail rounded float-start tblpic',
                    'dir' => 'uploads',
                    'link' => [
                        'controller' => 'Images',
                        'action' => 'view',
                        'param' => 'id',
                    ],
                ],
            ],
            [
                'name' => 'Layoutcheck',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'name' => 'original',
                        'action' => 'showoriginal',
                        'param' => 'id',
                        'btncolor' => 'success',
                    ],
                    [
                        'name' => 'responsive',
                        'action' => 'showresponsive',
                        'param' => 'id',
                        'btncolor' => 'success',
                    ],
                ],
            ],
            [
                'name' => 'Actions',
                'class' => 'actions',
                'actionitems' => [
                    [
                        'action' => 'edit',
                        'param' => 'id',
                    ],
                    [
                        'action' => 'delete',
                        'param' => 'id',
                        'postlink' => __('Are you sure you want to delete?'),
                    ],
                ],
            ],
        ];
        
        $sidenavitems = [
            ['action' => 'add'],
        ];
        $this->set(compact('sidenavitems', 'images', 'tblitems'));
    }

    /**
     * showoriginal method
     *
     * @param string|null $id Image id.
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function showresponsive($id = NULL)
    {
        try {
            $image = $this->Images->get($id, [
                'contain' => [],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Image do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        $imgitem = [
            'text' => 'uploads/' . $image->filename,
            'alt' => $image->title,
            'class' => 'img-fluid mx-auto d-block',     
        ];

        $this->viewBuilder()->setLayout('userlayout');
        $this->set(compact('image', 'imgitem'));
    }
    
    /**
     * showoriginal method
     *
     * @param string|null $id Image id.
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function showoriginal($id = NULL)
    {
        try {
            $image = $this->Images->get($id, [
                'contain' => [],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Image do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        $imgitem = [
            'text' => 'uploads/' . $image->filename,
            'alt' => $image->title,
            'class' => 'mx-auto d-block',     
        ];

        $this->viewBuilder()->setLayout('userlayout');
        $this->set(compact('image', 'imgitem'));
    }
    
    /**
     * View method
     *
     * @param string|null $id Image id.
     *
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = NULL)
    {
        try {
            $image = $this->Images->get($id, [
                'contain' => [],
            ]);
        } catch (RecordNotFoundException $e) {
            
            $this->Flash->info(__('Image do not exist.'), [
                'clear' => true,
                'params' => ['typ' => 'warning',],
            ]);
            return $this->redirect(['action' => 'index']);
        }
                
        
        $items = [
            [
                'label' => 'Name',
                'type'  => 'text',
                'text' => $image->title,
            ],
            [
                'text' => 'uploads/' . $image->filename,
                'type'  => 'image',
                'alt' => $image->title,
                'class' => 'img-thumbnail rounded float-start plantpic',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Images',
                'action' => 'index',
            ],
            [
                'action' => 'edit',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $image->title),
            ],
        ];

        $formtitle = 'layout';
        
        $this->set(compact('sidenavitems', 'image', 'items', 'formtitle'));
    }
    
    
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $image = $this->Images->newEmptyEntity();
        
        if ($this->request->is('post')) {
            
            $image = $this->Images->patchEntity($image, $this->request->getData());
                      
            if ( !$image->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'uploads';
                $imagename = $this->request->getData('image_file');
                $filename = $imagename->getClientFilename();

                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {
                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;

                    if ( $imagename ) {
                        $imagename->moveTo($targetPath);
                    }
                    $image->filename = $filename;
                }
            }
            
            if ($this->Images->save($image)) {
                
                $this->Flash->info(__('The image has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action'     => 'index',
                ]);
            } else {

                $this->Flash->info( __('The image could not be saved. Please, try again.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'danger',],
                ] );
            }
        }
        $items = [
            [
                'label' => 'Name',
                'field' => 'title',
                'type'  => 'text',
            ],
            [
                'label' => 'Image',
                'field' => 'image_file',
                'type'  => 'file',
            ],
        ];

        $sidenavitems = [
            [
                'name' => 'List all Images',
                'action' => 'index',
            ],
        ];
        $this->set(compact('sidenavitems', 'image', 'items'));
    }
   

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            $image = $this->Images->patchEntity($image, $this->request->getData());
        
            if ( !$image->getErrors() ) {

                $path = WWW_ROOT . 'img' . DS . 'uploads';
                $imagename = $this->request->getData('image_change');
//              debug($image);
//              exit;
                $filename = $imagename->getClientFilename();

                if ( strlen($filename) > 0 ) {

                    if ( !is_dir($path) ) {

                        mkdir($path, 0755);
                    }
                    $targetPath = $path . DS . $filename;
                    $imagename->moveTo($targetPath);

                    if ( isset($image->filename) ) {

                        $imagePath = $path . DS . $image->filename;

                        if ( file_exists($imagePath) ) {
                            unlink($imagePath);
                        }
                    }
                    $image->filename = $filename;
                }
            }

            if ( $this->Images->save($image) ) {

                $this->Flash->info( __('The image has been saved.'), [
                    'clear'  => TRUE,
                    'params' => ['typ' => 'success',],
                ] );
                return $this->redirect([
                    'action' => 'edit',
                    $id,
                ]);
            }
            $this->Flash->info( __('The image could not be saved. Please, try again.'), [
                'clear'  => TRUE,
                'params' => ['typ' => 'warning',],
            ] );
        }
        
        $items = [
            [
                'label' => 'Name',
                'field' => 'title',
                'type'  => 'text',
            ],
            [
                'label' => 'Upload Image',
                'field' => 'image_change',
                'type'  => 'file',
            ],
            [
                'label' => 'Current Image',
                'field' => 'image_current',
                'text' => 'uploads/' . $image->filename,
                'type' => 'image',
                'alt' => $image->title,
                'class' => 'img-thumbnail rounded float-start plantpic',
            ],
        ];
                
        $sidenavitems = [
            [
                'name' => 'List all Images',
                'action' => 'index',
            ],
            [
                'name' => 'Details',
                'action' => 'view',
                'param' => $id,
            ],
            [
                'action' => 'delete',
                'param' => $id,
                'postlink' => __('Are you sure you want to delete {0}?', $image->title),
            ],
        ];
        
        $this->set(compact('sidenavitems', 'image', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
