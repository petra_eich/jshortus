<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Image[]|\Cake\Collection\CollectionInterface $images
 * @var $pagecolor
 * @var $tblitems
 */

if ( count($images) == 0 ) { ?>

    <div class="card border-<?= $pagecolor; ?> mb-3">
        <div class="card-body text-dark">
            <h2>no data yet :)</h2>
        </div>
    </div> <?php
} else { ?>
    
    <?= $this->element('formsearch'); ?>

    <div class="card border-<?= $pagecolor; ?> mb-3">
        <?= $this->element('tablecontent', ['tbldata' => $images, 'tblitems' => $tblitems,]); ?>

        <div class="card-footer border-<?= $pagecolor; ?>">
            <small class="text-muted"><?= $this->element('pagination'); ?></small>
        </div>
    </div> <?php
} ?>
