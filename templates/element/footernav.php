<?php   
use Cake\Core\Configure;
?>
<footer>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
      <span class="navbar-text">
        CakePHP <?= Configure::version(); ?> / PHP <?= PHP_VERSION; ?>
      </span>
    </nav>
</footer>

