<?php
/**
 * @var $pagecolor
 * @var $created
 */ ?>

<div class="card-footer border-<?= $pagecolor; ?>">
    <small class="text-muted"><?= __('Created') . ': ' . h(date('d.m.Y H:i', strtotime($created))); ?></small>
</div>
