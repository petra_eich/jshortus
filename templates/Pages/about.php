<div class="card border-success mb-3">
    <img src="..." class="card-img-top" alt="...">
    
    <div class="card-body text-success">
      <h5 class="card-title">Hallo, <br>
            ich bin Petra.
      </h5>
      <p class="card-text">
          Als leidenschaftliche PHP-Entwicklerin plane, entwickle und realisiere ich erfolgreich anspruchsvolle IT-Projekte.
      </p>    
      <p class="card-text">
          Meine Kernkompetenzen lassen sich folgendermassen zusammenfassen:
      </p>
    </div>

    <ul class="list-group list-group-flush">
       <li class="list-group-item">Umsetzung von Projekten in PHP individuellen nach Anforderung</li>
        <li class="list-group-item">Planung und Konzeption von Anwendungen aus verschiedensten Bereichen (E-Commerce, Controlling, Personalplanung, Logistik)</li>
        <li class="list-group-item">Analyse und Weiterentwicklung von bestehenden Projekten</li>
        <li class="list-group-item">Entwicklung von dynamischen Webseiten sowie einzelner Applikationen und Module</li>
        <li class="list-group-item">Datenbankdesign und Optimierung der Datenbanken</li>
    </ul>      
</div>
